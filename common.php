<?php
function loadClassPhp(string $directory): void
{
    if (is_dir($directory)) {
        $scan = scandir($directory);
        if ($scan !== false) {
            unset($scan[0], $scan[1]); // unset . and ..
            foreach ($scan as $file) {
                $filePath = $directory . '/' . $file;
                if (is_dir($filePath)) {
                    loadClassPhp($filePath);
                } elseif (str_contains($file, '.php')) {
                    include_once $filePath;
                }
            }
        }
    }
}

function readStdIn(string $stream = 'php://stdin'): array
{
    $holder = [];
    $stdin = fopen($stream, 'r');
    if ($stdin !== false) {
        while (($line = fgetcsv($stdin, 100, ' ')) !== false) {
            $holder[] = $line;
        }
        fclose($stdin);
    }
    return $holder;
}
