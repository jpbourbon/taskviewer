<?php
namespace App\Assertions;

class IsHHOrAsterisk
{
    /**
     * @param string $time
     * @return bool
     */
    public function __invoke(string $time): bool
    {
        return preg_match('/^(?:[0-9]|2[0-3]|[01][0-9]|\*)$/', $time) === 1;
    }
}
