<?php
namespace App\Assertions;

class IsHHmm
{
    /**
     * @param string $time
     * @return bool
     */
    public function __invoke(string $time): bool
    {
        return preg_match('/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/', $time) === 1;
    }
}
