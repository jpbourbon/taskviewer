<?php
namespace App\Assertions;

class IsArray
{
    /**
     * @param mixed $item
     * @return bool
     */
    public function __invoke(mixed $item): bool
    {
        return is_array($item);
    }
}
