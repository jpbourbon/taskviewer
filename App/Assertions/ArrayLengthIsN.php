<?php
namespace App\Assertions;

class ArrayLengthIsN
{
    /**
     * @param array $array
     * @param int $n
     * @return bool
     */
    public function __invoke(array $array, int $n): bool
    {
        return count($array) === $n;
    }
}
