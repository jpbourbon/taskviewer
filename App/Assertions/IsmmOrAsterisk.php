<?php
namespace App\Assertions;

class IsmmOrAsterisk
{
    /**
     * @param string $time
     * @return bool
     */
    public function __invoke(string $time): bool
    {
        return preg_match("/^(?:[0-9]|[0-5][0-9]|\*)$/", $time);
    }
}