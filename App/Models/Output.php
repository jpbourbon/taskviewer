<?php
namespace App\Models;

use App\Contracts\OutputInterface;

class Output implements OutputInterface
{
    public function dd(string $string): void
    {
        $this->writeToConsole($string);
        $this->die();
    }

    public function writeToConsole(string $string): void
    {
        echo "$string\n";
    }

    private function die(): never
    {
        die();
    }
}
