<?php
namespace App\Processes;

use App\Assertions\ArrayLengthIsN;
use App\Assertions\IsArray;
use App\Assertions\IsHHOrAsterisk;
use App\Assertions\IsmmOrAsterisk;
use App\Contracts\OutputInterface;
use App\Contracts\ProcessInterface;

class ValidateFileArgument implements ProcessInterface
{
    public const MESSAGE = "INPUT FILE ERROR\nFile format is invalid\n";
    public const MESSAGE_COUNT = 'Please confirm all lines have 3 columns separated by a blanck space\n';
    public const MESSAGE_MM = 'Please ensure the first filed is minutes in mm format or and *';
    public const MESSAGE_HH = 'Please ensure the second field is hours in 24h format or an *';
    public const MESSAGE_NOT_ARRAY = 'Please make sure you load a file by STDIN';
    private const ARG_POSITION = 2;
    private const MM_POSITION = 0;
    private const HH_POSITION = 1;
    private const DEFAULT_N = 3;

    public function __construct(private OutputInterface &$output)
    {
    }

    public function __invoke(array $array): bool
    {
        $file = $array[self::ARG_POSITION];

        $isArray = new IsArray();
        $arrayLengthIsN = new ArrayLengthIsN();
        $isHHOrAsterisk = new IsHHOrAsterisk();
        $ismmOrAsterisk = new IsmmOrAsterisk();

        if (!$isArray($file)) {
            $this->output->dd(self::MESSAGE . self::MESSAGE_NOT_ARRAY);
            return false;
        }

        foreach ($file as $line) {
            if (!$arrayLengthIsN($line, self::DEFAULT_N)) {
                $this->output->dd(self::MESSAGE . self::MESSAGE_COUNT);
                return false;
            }

            if (!$ismmOrAsterisk($line[self::MM_POSITION])) {
                $this->output->dd(self::MESSAGE . self::MESSAGE_MM);
                return false;
            }

            if (!$isHHOrAsterisk($line[self::HH_POSITION])) {
                $this->output->dd(self::MESSAGE . self::MESSAGE_HH);
                return false;
            }
        }

        return true;
    }
}
