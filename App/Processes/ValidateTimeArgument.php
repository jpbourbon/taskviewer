<?php
namespace App\Processes;

use App\Assertions\IsHHmm;
use App\Contracts\OutputInterface;
use App\Contracts\ProcessInterface;

class ValidateTimeArgument implements ProcessInterface
{
    public const MESSAGE = "ARGUMENT FORMAT ERROR\nArgument 1 must be time in HH:mm 24h format\n";
    private const ARG_POSITION = 1;

    public function __construct(private OutputInterface &$output)
    {
    }

    public function __invoke(array $array): bool
    {
        $assertion = new IsHHmm();

        if (!$assertion($array[self::ARG_POSITION])) {
            $this->output->dd(self::MESSAGE);
            return false;
        }

        return true;
    }
}
