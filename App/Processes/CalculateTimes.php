<?php
namespace App\Processes;

use App\Actions\BreakHHmmAsArray;
use App\Actions\CombineHHmm;
use App\Actions\GetDayFromTimeDiff;
use App\Contracts\OutputInterface;
use App\Contracts\ProcessInterface;
use App\Actions\ParseTimeUnitsRelativeToHHmm;

class CalculateTimes implements ProcessInterface
{
    private const HHMM_POSITION = 1;
    private const FILE_POSITION = 2;
    private const M_POSITION = 0;
    private const H_POSITION = 1;
    private const PATH_POSITION = 2;

    public function __construct(private OutputInterface &$output)
    {
    }

    public function __invoke(array $args): void
    {
        $breakHHmmAsArray = new BreakHHmmAsArray();
        $combineHHmm = new CombineHHmm();
        $getDayFromTimeDiff = new GetDayFromTimeDiff();
        $parseTimeUnitsRelativeToHHmm = new ParseTimeUnitsRelativeToHHmm();

        [$refHH, $refmm] = $breakHHmmAsArray($args[self::HHMM_POSITION]);

        foreach ($args[self::FILE_POSITION] as $line) {
            $parsedTime = $parseTimeUnitsRelativeToHHmm(
                $line[self::H_POSITION],
                $line[self::M_POSITION],
                $args[self::HHMM_POSITION],
                $breakHHmmAsArray,
                $combineHHmm
            );

            $finalArray = [
                $parsedTime,
                $getDayFromTimeDiff($args[self::HHMM_POSITION], $parsedTime),
                '-',
                $line[self::PATH_POSITION],
            ];

            $final = implode(' ', $finalArray);
            $this->output->writeToConsole($final);
        }
    }
}
