<?php
namespace App\Processes;

use App\Assertions\ArrayLengthIsN;
use App\Contracts\OutputInterface;
use App\Contracts\ProcessInterface;

class ValidateArgumentCount implements ProcessInterface
{
    public const MESSAGE = "ARGUMENT COUNT ERROR\nPlease provide:\n1) time in HH:mm 24h format\n2) Valid text file as STDIN";
    public const DEFAULT_N = 3;

    public function __construct(private OutputInterface& $output)
    {
    }

    public function __invoke(array $array): bool
    {
        $assertion = new ArrayLengthIsN();

        if (!$assertion($array, self::DEFAULT_N)) {
            $this->output->dd(self::MESSAGE);
        }

        return true;
    }
}
