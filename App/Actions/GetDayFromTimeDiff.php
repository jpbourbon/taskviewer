<?php
namespace App\Actions;

class GetDayFromTimeDiff
{
    /**
     * @param string $timeA
     * @param string $timeB
     * @return string
     */
    public function __invoke(string $timeA, string $timeB): string
    {
        return strtotime($timeA) <= strtotime($timeB)
            ? 'today'
            : 'tomorrow';
    }
}
