<?php
namespace App\Actions;

class CombineHHmm
{
    /**
     * @param string $hours
     * @param string $minutes
     * @return string
     */
    public function __invoke(string $hours, string $minutes): string
    {
        return "{$hours}:{$minutes}";
    }
}
