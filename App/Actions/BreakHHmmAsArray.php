<?php
namespace App\Actions;

class BreakHHmmAsArray
{
    /**
     * @param string $time
     * @return array{0: int, 1: int}
     */
    public function __invoke(string $time): array
    {
        $exploded = explode(':', $time);
        return [
            (int) $exploded[0],
            (int) $exploded[1],
        ];
    }
}
