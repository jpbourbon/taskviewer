<?php
namespace App\Actions;

class ParseTimeUnitsRelativeToHHmm
{
    /**
     * @param string $hours
     * @param string $minutes
     * @param string $time
     * @param callable(string): array{0: int, 1: int} $breaker
     * @param callable(string, string): string $joine
     * @return string
     */
    public function __invoke(string $hours, string $minutes, string $time, callable $breaker, callable $joiner): string
    {   
        [$refHH, $refmm] = $breaker($time);

        if ($hours !== "*" && $minutes !== "*") {
            $joined = $joiner($hours, $minutes);
            return $this->formatReturn($joined);
        }

        if ($hours === "*" && $minutes === "*") {
            return $this->formatReturn($time);
        }

        if ($hours === "*") {
            if ($refmm <= $minutes) {
                $joined = $joiner($refHH, $minutes);
                return $this->formatReturn($joined);
            } else {
                $joined = $joiner($refHH, $minutes);
                $added = strtotime("+1 hour", strtotime($joined));
                return $this->formatReturn($added, false);
            }
        }

        if ($minutes === "*") {
            if ($refHH !== $hours) {
                $joined = $joiner($hours, "00");
                return $this->formatReturn($joined);
            } else {
                $joined = $joiner($hours, $refmm);
                $added = strtotime("+1 minute", strtotime($joined));
                return $this->formatReturn($added, false);
            }
        }
    }

    /**
     * @param string $joined
     * @return string
     */
    private function formatReturn(string $string, $convert = true): string
    {
        $string = $convert ? strtotime($string) : $string;

        return ltrim(date('H:i', $string), 0);
    }
}