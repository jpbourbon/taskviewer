<?php
namespace App\Operations;

use App\Assertions\IsArgumentCountValid;

class Execute extends AbstractOperation
{
    /**
     * @param callable[] $functions
     */
    public function __construct(array $functions)
    {
        parent::__construct($functions);
    }
}
