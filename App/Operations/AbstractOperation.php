<?php
namespace App\Operations;

abstract class AbstractOperation
{
    /** @var callable[] */
    protected array $functions;

    /**
     * @param callable[] $functions
     */
    public function __construct(array $functions)
    {
        $this->functions = $functions;
    }

    /**
     * @param array $args
     * @return bool
     */
    public function __invoke(array $args): bool
    {
        array_walk($this->functions, function(callable $function) use ($args): void {
            $function($args);
        });

        return true;
    }
}
