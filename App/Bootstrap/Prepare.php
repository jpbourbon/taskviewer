<?php
namespace App\Bootstrap;

use App\Assertions\IsArgumentCountValid;
use App\Models\Output;
use App\Processes\CalculateTimes;
use App\Processes\ValidateArgumentCount;
use App\Processes\ValidateFileArgument;
use App\Processes\ValidateTimeArgument;

class Prepare
{
    /**
     * @return array{0: array<ValidateArgumentCount|ValidateTimeArgument|ValidateFileArgument>, 1: array<CalculateTimes>}
     */
    public function __invoke(): array
    {
        $output = new Output();
        $valFunctions = [
            new ValidateArgumentCount($output),
            new ValidateTimeArgument($output),
            new ValidateFileArgument($output),
        ];

        $execFunctions = [
            new CalculateTimes($output),
        ];

        return [$valFunctions, $execFunctions];
    }
}
