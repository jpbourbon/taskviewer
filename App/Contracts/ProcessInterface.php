<?php
namespace App\Contracts;

interface ProcessInterface
{
    public function __construct(OutputInterface &$output);

    public function __invoke(array $array);
}