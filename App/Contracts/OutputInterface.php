<?php
namespace App\Contracts;

interface OutputInterface
{
    public function dd(string $string): void;

    public function writeToConsole(string $string): void;
}
