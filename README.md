# Taskviewer

## Installationa
To be used in *nix systems.\
Developed and tested in a macOS Catalina 10.15.7 with stock PHP 7.3\
Updated for compatibility with PHP 8.3\
The permission should have been preserved by the compression, but in the case they weren't please aply\
```bash
chmod +x taskviewer
chmod +x taskviewer_test
```
After uncompressing the file to a folder.

## Running
For the main application:\
```bash
./taskviewer <HH:mm> < <input file>
```
For the unit tests:\
```bash
./taskviewer_test
```

## Design decisions
Given the stated rules against using 3rd party libraries, I included my own take on unit testing.\
These tests could be easily adapted to run in PHPUnit as well.\
I tend to like having very small, granular  classes that follow the Single responsibility SOLID principle. I find this is the best way to have testable and reusable code.\
I also take advantage of the `__invoke` method in PHP, which allow a class to be treated as a callable function, bringing some interesting possibilities to code organisation and making it more declarative.\
Regarding business logic, I opted to separate validation of the inputs from the calculation and output of the result instead of doing so in a single iteration while reading the stream.\
This makes for a much cleaner code, where separation of concern is visibly enforced.