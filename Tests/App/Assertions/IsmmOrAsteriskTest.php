<?php
namespace Tests\App\Assertions;

use App\Assertions\IsmmOrAsterisk as Assertion;
use Tests\BaseTest;

class IsmmOrAsteriskTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * The minutes are well formad
     */
    public function testMinutesAreCorrectlyFormed()
    {
        $times = [
            "*",
            "1",
            "01",
            "59"
        ];

        $assertion = new Assertion();

        foreach ($times as $time) {
            $result = $assertion($time);

            $this->assertTrue($result, __FUNCTION__);
        }
    }

    /**
     * Minutes are not well set
     */
    public function testMinutesAreIncorrect()
    {
        $times = [
            "30:30",
            "a",
            "60",
            "-1"
        ];

        $assertion = new Assertion();

        foreach ($times as $time) {
            $result = $assertion($time);

            $this->assertFalse($result, __FUNCTION__);
        }
    }
}