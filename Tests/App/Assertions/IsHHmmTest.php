<?php
namespace Tests\App\Assertions;

use App\Assertions\IsHHmm as Assertion;
use Tests\BaseTest;

class IsHHmmTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * The time follos a 24-hour HH:mm format
     */
    public function testTimeInCorrectFormat()
    {
        $times = [
            "10:30",
            "00:00",
            "07:11",
            "23:59"
        ];

        $assertion = new Assertion();

        foreach ($times as $time) {
            $result = $assertion($time);

            $this->assertTrue($result, __FUNCTION__);
        }
    }

    /**
     * Time is not in the expected format
     */
    public function testTimeInInorrectFormat()
    {
        $times = [
            "30:30",
            "00-00",
            "0711",
            "23:60"
        ];

        $assertion = new Assertion();

        foreach ($times as $time) {
            $result = $assertion($time);

            $this->assertFalse($result, __FUNCTION__);
        }
    }
}