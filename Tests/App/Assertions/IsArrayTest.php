<?php
namespace Tests\App\Assertions;

use App\Assertions\IsArray as Assertion;
use Tests\BaseTest;

class IsArrayTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * Asserts the variable is an array
     */
    public function testIsAnArray()
    {
        $vars = [[], array()];

        $assertion = new Assertion();

        foreach ($vars as $var) {
            $result = $assertion($var);

            $this->assertTrue($result, __FUNCTION__);
        }
    }

    /**
     * Asserts the variable is not an array
     */
    public function testIsNotAnArray()
    {
        $vars = ["a", 1, new \stdClass(), 1.1, true];

        $assertion = new Assertion();

        foreach ($vars as $var) {
            $result = $assertion($var);

            $this->assertFalse($result, __FUNCTION__);
        }
    }
}