<?php
namespace Tests\App\Assertions;

use App\Assertions\IsHHOrAsterisk as Assertion;
use Tests\BaseTest;

class IsHHOrAsteriskTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * The hours are well formed
     */
    public function testHoursInCorrectFormat()
    {
        $times = [
            "*",
            "1",
            "01",
            "23"
        ];

        $assertion = new Assertion();

        foreach ($times as $time) {
            $result = $assertion($time);

            $this->assertTrue($result, __FUNCTION__);
        }
    }

    /**
     * Hours are not in the expected format
     */
    public function testHoursInInorrectFormat()
    {
        $times = [
            "30:30",
            "a",
            "24",
            "-1"
        ];

        $assertion = new Assertion();

        foreach ($times as $time) {
            $result = $assertion($time);

            $this->assertFalse($result, __FUNCTION__);
        }
    }
}