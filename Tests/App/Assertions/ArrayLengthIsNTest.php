<?php
namespace Tests\App\Assertions;

use App\Assertions\ArrayLengthIsN as Assertion;
use Tests\BaseTest;

class ArrayLengthIsNTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * Success if array length is same as count
     */
    public function testSuccess(): void
    {
        $array = range(1,3);
        $count = 3;

        $assertion = new Assertion();

        $result = $assertion($array, $count);

        $this->assertTrue($result, __FUNCTION__);
    }

    /**
     * Fails if array count is not equal to expected
     */
    public function testFailure(): void
    {
        $array = range(1,2);
        $count = 3;

        $assertion = new Assertion();

        $result = $assertion($array, $count);

        $this->assertFalse($result, __FUNCTION__);
    }
}