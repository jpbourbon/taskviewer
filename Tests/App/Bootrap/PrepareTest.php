<?php
namespace Tests\App\Bootstrap;

use App\Bootstrap\Prepare;
use Tests\BaseTest;

class PrepareTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * Returns the arrays with processes
     */
    public function testIsAnArray()
    {
        $expected = [3, 1];

        $prepare = new Prepare();

        $result = $prepare();

        $this->assertInternalType('array', $result, __FUNCTION__);
        $this->assertCount(2, $result, __FUNCTION__);
        $this->assertInternalType('array',$result[0], __FUNCTION__);
        $this->assertInternalType('array',$result[1], __FUNCTION__);
        $this->assertCount($expected[0], $result[0], __FUNCTION__);
        $this->assertCount($expected[1], $result[1], __FUNCTION__);
    }
}