<?php
namespace Tests\App\Processes;

use App\Processes\ValidateArgumentCount as Process;
use Tests\BaseTest;
use Tests\Output;

class ValidateArgumentCountTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * Tests the validation of argument count is passed
     */
    public function testSuccess(): void
    {
        $output = new Output();
        $array = range(1,3);

        $process = new Process($output);

        $result = $process($array);

        $this->assertTrue($result, __FUNCTION__);
    }

    /**
     * Tests the validation of argument count fails with appropriate message
     */
    public function testCanFailGracefully(): void
    {
        $output = new Output();
        $array = [];

        $process = new Process($output);
        $process($array);

        $result = $output->getHolder();
        $expected = $process::MESSAGE;


        $this->assertEquals($expected, $result, __FUNCTION__);
    }
}