<?php
namespace Tests\App\Processes;

use App\Processes\ValidateFileArgument as Process;
use Tests\BaseTest;
use Tests\Output;

class ValidateFileArgumentTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * Tests the contents of the file are valid
     */
    public function testFileContentsAreValid(): void
    {
        $output = new Output();
        $array = [0, 0, [[0,0,0]]];

        $process = new Process($output);

        $result = $process($array);

        $this->assertTrue($result, __FUNCTION__);
    }

    /**
     * Tests the file contents are invalid: incorrect number of columns
     */
    public function testCanFailGracefully1(): void
    {
        $output = new Output();
        $array = [0, 0, [[]]];

        $process = new Process($output);
        $process($array);

        $result = $output->getHolder();
        $expected = $process::MESSAGE.$process::MESSAGE_COUNT;


        $this->assertEquals($expected, $result, __FUNCTION__);
    }

    /**
     * Tests the file contents are invalid: minutes are invalid
     */
    public function testCanFailGracefully2(): void
    {
        $output = new Output();
        $array = [0, 0, [[660,0,0]]];

        $process = new Process($output);
        $process($array);

        $result = $output->getHolder();
        $expected = $process::MESSAGE.$process::MESSAGE_MM;


        $this->assertEquals($expected, $result, __FUNCTION__);
    }

    /**
     * Tests the file contents are invalid: hours are invalid
     */
    public function testCanFailGracefully3(): void
    {
        $output = new Output();
        $array = [0, 0, [[59,24,0]]];

        $process = new Process($output);
        $process($array);

        $result = $output->getHolder();
        $expected = $process::MESSAGE.$process::MESSAGE_HH;


        $this->assertEquals($expected, $result, __FUNCTION__);
    }
}