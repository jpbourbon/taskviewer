<?php
namespace Tests\App\Processes;

use App\Processes\ValidateTimeArgument as Process;
use Tests\BaseTest;
use Tests\Output;

class ValidateTimeArgumentTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * Tests the validation of format is passed
     */
    public function testArgumentInRightFormat(): void
    {
        $output = new Output();
        $array = [0,'12:12',0];

        $process = new Process($output);

        $result = $process($array);

        $this->assertTrue($result, __FUNCTION__);
    }

    /**
     * Tests the validation of time format fails with appropriate message
     */
    public function testCanFailGracefully(): void
    {
        $output = new Output();
        $array = [0, '1234', 0];

        $process = new Process($output);
        $process($array);

        $result = $output->getHolder();
        $expected = $process::MESSAGE;


        $this->assertEquals($expected, $result, __FUNCTION__);
    }
}