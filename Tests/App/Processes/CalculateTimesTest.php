<?php
namespace Tests\App\Processes;

use App\Processes\CalculateTimes as Process;
use Tests\BaseTest;
use Tests\Output;

class CalculateTimesTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * Can successfuly generate the required output
     */
    public function testSuccess(): void
    {
        $output = new Output();
        $array = [
            'abc',
            '12:00',
            [
                ['29', '12', 'abc',],
                ['59', '11', 'foo'],
                ['*', '12', 'bar'],
                ['1', '*', 'baz'],
                ['9', '1', 'cuz'],
                ['*', '*', 'xyz']
            ],
        ];

        $expected = [
            '12:29 today - abc',
            '11:59 tomorrow - foo',
            '12:00 today - bar',
            '12:01 today - baz',
            '1:09 tomorrow - cuz',
            '12:00 today - xyz',
        ];

        $process = new Process($output);

        $process($array);

        $result = $output->getBucket();

        $this->assertTrue($expected == $result, __FUNCTION__);
    }
}