<?php
namespace Tests\App\Operations;

use App\Bootstrap\Prepare;
use App\Operations\Execute as Operation;
use Tests\BaseTest;
use Tests\Output;

class ExecuteTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * Can validate the right format of input
     */
    public function testCanValidateCorrectInput()
    {
        $prepare   = new Prepare();
        $functions = $prepare();

        $array = [
            'abc',
            '12:00',
            [
                ['29', '12', 'abc',],
                ['59', '11', 'foo'],
                ['*', '12', 'bar'],
                ['1', '*', 'baz'],
                ['9', '1', 'cuz'],
                ['*', '*', 'xyz']
            ],
        ];

        $operation = new Operation($functions[1]);
        $result    = $operation($array);

        $this->assertTrue($result, __FUNCTION__);
    }
}