<?php
namespace Tests\App\Actions;

use App\Actions\BreakHHmmAsArray as Action;
use Tests\BaseTest;

class BreakHHmmAsArrayTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * Array is returned with 2 elements containing hours and minutes
     */
    public function testCanCreateArrayFromTime(): void
    {
        $time = "14:30";

        $expected = [14, 30];

        $action = new Action();

        $result = $action($time);

        $this->assertInternalType('array', $result, __FUNCTION__);
        $this->assertCount(2, $result, __FUNCTION__);
        $this->assertEquals($expected, $result, __FUNCTION__);
    }
}