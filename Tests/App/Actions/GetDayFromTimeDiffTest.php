<?php
namespace Tests\App\Actions;

use App\Actions\GetDayFromTimeDiff as Action;
use Tests\BaseTest;

class GetDayFromTimeDiffTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * Proves today is returned if timeA is less or equal to timeB
     */
    public function testTodayIsReturned(): void
    {
        $timeA = "14:30";
        $timeBArray = [
            "20:00",
            "18:14",
            "23:53",
            $timeA,
        ];

        $expected = "today";

        $action = new Action();

        foreach ($timeBArray as $timeB) {
            $result = $action($timeA, $timeB);

            $this->assertInternalType('string', $result, __FUNCTION__);
            $this->assertEquals($expected, $result, __FUNCTION__);
        }
    }

    /**
     * Proves tomorrow is returned if timeA is more than timeB
     */
    public function testTomorrowIsRetu(): void
    {
        $timeA = "14:30";
        $timeBArray = [
            "00:00",
            "08:14",
            "10:20",
            "14:29",
        ];

        $expected = "tomorrow";

        $action = new Action();

        foreach ($timeBArray as $timeB) {
            $result = $action($timeA, $timeB);

            $this->assertInternalType('string', $result, __FUNCTION__);
            $this->assertEquals($expected, $result, __FUNCTION__);
        }
    }


}