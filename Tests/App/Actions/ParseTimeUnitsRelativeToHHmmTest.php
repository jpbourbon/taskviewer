<?php
namespace Tests\App\Actions;

use App\Actions\BreakHHmmAsArray;
use App\Actions\CombineHHmm;
use App\Actions\ParseTimeUnitsRelativeToHHmm as Action;
use Tests\BaseTest;

class ParseTimeUnitsRelativeToHHmmTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * Can parse the * to be the same as the reference time unit
     */
    public function testCanParseAsterisk(): void
    {
        $unitsArray = [
            ["*", "*"],
            ["*", "15"],
            ["*", "03"],
            ["20", "*"],
            ["19", "45"],
        ];
        $time    = "15:04";
        $breaker = new BreakHHmmAsArray();
        $joiner  = new CombineHHmm();

        $expected = [
            $time,
            "15:15",
            "16:03",
            "20:00",
            "19:45",

        ];

        $action = new Action();

        foreach ($unitsArray as $key => $units) {
            $result = $action($units[0], $units[1], $time, $breaker, $joiner);

            $this->assertInternalType('string', $result, __FUNCTION__ );
            $this->assertEquals($expected[$key], $result, __FUNCTION__);
        }
    }
}