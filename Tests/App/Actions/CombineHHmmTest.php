<?php
namespace Tests\App\Actions;

use App\Actions\CombineHHmm as Action;
use Tests\BaseTest;

class CombineHHmmTest extends BaseTest
{
    /**
     * Invocation after class is instantiated
     */
    public function __invoke()
    {
        $methods = get_class_methods(self::class);

        $this->setup(self::class, $methods);
    }

    /**
     * Time string can be created from the combination of hours and minutes
     */
    public function testCanCreateTimeFromArray(): void
    {
        $hoursArray = ["00", 10, 15, 23];
        $minutesArray = ["00", "01", 15, "05"];

        $expected = ["00:00", "10:01", "15:15", "23:05"];

        $action = new Action();

        for ($i=0;$i<count($hoursArray);$i++) {
            $result = $action($hoursArray[$i], $minutesArray[$i]);

            $this->assertInternalType('string', $result, __FUNCTION__);
            $this->assertEquals($expected[$i], $result, __FUNCTION__);
        }
    }
}