<?php
namespace Tests;

use App\Contracts\OutputInterface;

class Output implements OutputInterface
{
    private $bucket = [];
    private $holder = "";

    /**
     * @param string $string
     */
    public function dd(string $string): void
    {
        $this->holder = $string;
    }

    /**
     * @param string $string
     */
    public function writeToConsole(string $string): void
    {
        $this->bucket []= $string;
    }

    /**
     * @return string
     */
    public function getHolder(): ?string
    {
        return $this->holder;
    }

    /**
     * @return array
     */
    public function getBucket(): array
    {
        return $this->bucket;
    }
}