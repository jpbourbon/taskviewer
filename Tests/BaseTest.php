<?php
namespace Tests;

class BaseTest
{
    /**
     * @param $className
     * @param $methods
     */
    public function setup($className, $methods)
    {
        echo "\n=== $className ===\n\n";

        $this->execute($methods);
    }
    /**
     * @param bool $result
     * @param string $description
     */
    protected function assertTrue(bool $result, string $description): void
    {
        $description .= " ---> ".__FUNCTION__."\n";
        $result === true ? $this->success($description) : $this->fail($description);
    }

    /**
     * @param bool $result
     * @param string $description
     */
    protected function assertFalse(bool $result, string $description): void
    {
        $description .= " ---> ".__FUNCTION__."\n";
        $result === false ? $this->success($description) : $this->fail($description);
    }

    /**
     * @param $expected
     * @param $result
     * @param string $description
     * @throws \Exception
     */
    protected function assertEquals($expected, $result, string $description): void
    {
        $description .= " ---> ".__FUNCTION__."\n";
        $expected === $result ? $this->success($description) : $this->fail($description);
    }

    /**
     * @param $expected
     * @param $result
     * @param string $description
     * @throws \Exception
     */
    protected function assertNotEquals($expected, $result, string $description): void
    {
        $description .= " ---> ".__FUNCTION__."\n";
        $expected !== $result ? $this->success($description) : $this->fail($description);
    }

    /**
     * @param int $expected
     * @param array $result
     * @param string $description
     * @throws \Exception
     */
    protected function assertCount(int $expected, array $result, string $description): void
    {
        $description .= " ---> ".__FUNCTION__."\n";
        $expected === count($result) ? $this->success($description) : $this->fail($description);
    }

    /**
     * @param string $expected
     * @param $result
     * @param string $description
     * @throws \Exception
     */
    protected function assertInstanceOf(string $expected, $result, string $description): void
    {
        $description .= " ---> ".__FUNCTION__."\n";
        in_array($expected,class_implements($result)) ? $this->success($description) : $this->fail($description);
    }

    /**
     * @param string $expected
     * @param $result
     * @param string $description
     * @throws \Exception
     */
    protected function assertInternalType(string $expected, $result, string $description): void
    {
        $description .= " ---> ".__FUNCTION__."\n";
        $expected == gettype($result) ? $this->success($description) : $this->fail($description);
    }

    /**
     * @param string $description
     */
    private function success(string $description): void
    {
        echo "OK --> $description";
    }

    /**
     * @param string $description
     */
    /**
     * @param string $description
     */
    private function fail(string $description): void
    {
        echo "FAIL --> $description\n";
        echo "\nA test has failed!\nPlease check stack trace below.\n";
        throw new \Exception();
    }

    /**
     * @param array $methods
     */
    protected function execute(array $methods): void
    {
        $testMethods = array_filter($methods, function($v) {
            return substr($v, 0, 4) == 'test';
        });
        foreach ($testMethods as $test) {
            $this->$test();
        }
    }
}